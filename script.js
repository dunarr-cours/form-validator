
// récuperation des elements HTML
const buttonElement = document.getElementById("submit-btn")
const usernameInput = document.getElementById("username")
const passwordInput = document.getElementById("password")
const confirmPasswordInput = document.getElementById("confirm-password")
const telInput = document.getElementById("tel")
const mailInput = document.getElementById("mail")
const messageElement = document.getElementById("message")

//ajout de l'event listener
buttonElement.addEventListener("click", validateForm)

function validateForm(){
    // Récupération des valeures des inputs (/!\ bien le faire dans la fonction)
    const username = usernameInput.value
    const password = passwordInput.value
    const confirmPassword = confirmPasswordInput.value
    const tel = telInput.value
    const mail = mailInput.value

    // initialisation de errors
    var errors = []

    // Si mon username est vide:
    if (username === ""){
        // ajouter une erreur
        errors.push("Identifiant obligatoire")
    }

    if (username.length < 8){
        // identifiant pas assez long
        errors.push("Identifiant trop court")
    }

    if (password === ""){
        // ajouter une erreur
        errors.push("Mot de passe obligatoire")
    }


    if (password.length < 8){
        // mdp pas assez long
        errors.push("Mot de passe trop court")
    }

    // if (password.match(/[0-9]/))
    // on n'a pas de 0 ni de 1 ni de 2 ni de 3 .......
    if(
        !password.includes(0)
        && !password.includes(1)
        && !password.includes(2)
        && !password.includes(3)
        && !password.includes(4)
        && !password.includes(5)
        && !password.includes(6)
        && !password.includes(7)
        && !password.includes(8)
        && !password.includes(9)
    ){
        //alors on affiche une erreure
        errors.push("Le mot de passe doit contenir au moins un chiffre")
    }

    if(
        !password.includes("%")
        && !password.includes("^")
        && !password.includes("$")
        && !password.includes("@")
    ){
        //alors on affiche une erreure
        errors.push("Le mot de passe doit contenir au moin un caractère spécial")
    }

    // je verifie si la confirmation de mdp est differente du mdp
    if (confirmPassword !== password) {
        // dans ce cas, j'affiche une erreure
        errors.push("Les mots de passe ne correspondent pas")
    }
    // if (isN  aN(tel)){
    // if (!tel.match(/^[0-9]{10}$/)){
    if (!verifTel(tel) || tel.length !== 10){
        errors.push("Le téléphone est invalide")
    }

    atPos = mail.indexOf("@")
    // je recupère la pos de @
    // je recupère la pos du dernier point (dans le cas ou il y en a plusieurs)
    dotPos = mail.lastIndexOf(".")

    if(
        !mail.includes("@")
        || !mail.includes(".")
        || atPos > dotPos
    ){
        // je fais une erreure
        // si il n'y a pas de @
        // de point
        // ou si @ est après le point
        errors.push("Le mail est invalide")
    }

    // on efface les erreurs précédentes
    messageElement.innerHTML = ""

    if(errors.length === 0){
        // Je n'ai pas d'erreures
        // Je peu afficher le message de validation
        const successElement = document.createElement("div")
        // errorElement.className = "success"
        successElement.classList.add("success")
        successElement.innerHTML="compte créé"
        messageElement.appendChild(successElement)
    } else {
        // J'ai des erreures
        // je peu afficher mes message d'erreur
        const errorElement = document.createElement("div")
        // errorElement.className = "error"
        errorElement.classList.add("error")
        errorElement.innerHTML = errors.join('<br>')
        messageElement.appendChild(errorElement)


    }
}

// verifie si je n'ai que des chiffres
function verifTel(tel){
    // je décomppose mon tel en plusieures caractère
    for (char of tel){
        // je vérifie caractère par caractère si j'ai un chiffre
        if(["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"].includes(char)){
            // J'ai un chiffre
            // je ne fais pas de return true, car je n'ai pas encore vérifié les autres caractères
        } else {
            // J'ai une lettre
            // le tel n'est pas valide
            return false
        }
    }
    // Je ne suis pas encore sortie de la fonction
    // je n'ai donc pas de lettres
    // le tel est donc valide
    return true

}
